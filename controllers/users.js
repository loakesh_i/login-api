const Validation = require('../helpers/validation');
const UserService = require('../services/userService.js');
const logger = require('../config/logger');
const tokens = require('../helpers/tokens');

const UserController = {
  /**
   * @description Register User
   * @route POST /register
   * @param {object} req
   * @param {object} res
   */
  registerUser: async (req, res) => {
    const responseData = {};
    try {
      const user = {
        fullName: req.body.fullName,
        emailId: req.body.emailId,
        password: req.body.password,
      };
      const { error } = await Validation.registerUser(user);

      if (error) {
        throw new Error(error);
      }
      const result = await UserService.register(user);
      responseData.success = true;
      responseData.message = 'Successfully Registered User!';
      logger.info(responseData.message);
      res.status(200).send(responseData);
    } catch (error) {
      responseData.success = false;
      responseData.message = null;
      responseData.data = null;
      responseData.error = error.message;
      logger.error(error.message);
      console.log(error.stack);
      res.status(500).send(responseData);
    }
  },

  /**
   * @description LogIn User
   * @route POST /login
   * @param {object} req
   * @param {object} res
   */
  logInUser: async (req, res) => {
    const responseData = {};
    try {
      const user = {
        emailId: req.body.emailId,
        password: req.body.password,
      };
      const { error } = await Validation.logInUser(user);

      if (error) {
        throw new Error(error);
      }

      const result = await UserService.logIn(user);
      responseData.success = true;
      responseData.message = 'Successfully Logged In!';
      responseData.data = result;
      logger.info(responseData.message);

      const token = tokens.getSignedJwtToken(result._id);

      const options = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRE),
        httpOnly: true,
        secure: true,
      };
      responseData.token = token;
      res.status(200).cookie('token', token, options).send(responseData);
    } catch (error) {
      responseData.success = false;
      responseData.message = null;
      responseData.data = null;
      responseData.error = error.message;
      logger.error(error.message);
      res.status(500).send(responseData);
    }
  },
};

module.exports = UserController;
