const UserModel = require('../models/user.js');

const UserService = {
  register: async (user) => {
    const isUserPresent = await UserModel.findOne({
      emailId: user.emailId,
    });
    if (isUserPresent) {
      throw new Error(`User with email '${user.emailId}' already exists!`);
    }
    return await UserModel.createUser(user);
  },

  logIn: async (user) => {
    const { emailId, password } = user;
    
    const foundUser = await UserModel.findOne({ emailId });

    if (foundUser === null || !foundUser) {
      throw new Error(`User with email ${emailId}, Not Found!`);
    }

    // Check if password matches
    const isMatch = await foundUser.matchPassword(password);
    if (!isMatch) {
      throw new Error(`Incorrect Password`);
    }
    return foundUser;
  },
};

module.exports = UserService;
