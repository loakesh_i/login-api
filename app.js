const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const logger = require('./config/logger.js');
const routes = require('./routes/routes.js');
require('./config/dbEvents.js');
require('dotenv').config();

const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/', routes);

app.use('/', (req, res) => {
  res.status(200).send({
    success: true,
    message: 'BackEnd for User Login',
  });
});

// Handler for non-existence routes
app.use('*', (req, res) => {
  res.status(404).send({
    success: false,
    message: 'Page Does not exist',
  });
});

/**
 * Server
 */
const server = app.listen(process.env.PORT, () => {
  console.log(`Listening on port ${process.env.PORT}...`);
  logger.info(`Listening on port ${process.env.PORT}...`);
});

module.exports = { server };
