const express = require('express');
const router = express.Router();
const UserController = require('../controllers/users');

// Register user end point
router.post('/api/register-user', UserController.registerUser);

// Login user
router.post('/api/login', UserController.logInUser);

module.exports = router;
