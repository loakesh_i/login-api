const jwt = require('jsonwebtoken');

const tokens = {
  /**
   * @description Sign JWT
   * @param {payload} id
   * @returns jwt token
   */
  getSignedJwtToken: (id) => {
    return jwt.sign({ _id: id }, process.env.JWT_SECRET, {
      expiresIn: process.env.JWT_EXPIRE,
    });
  },

  /**
   * @description Verifies the payload in database and the decoded data from the token
   * @param {Token} requestToken
   */
  verifyEmailToken: (requestToken) => {
    return jwt.verify(requestToken, process.env.EMAIL_SECRET);
  },
};

module.exports = tokens;
