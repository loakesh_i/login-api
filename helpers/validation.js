const Joi = require('joi');

const Validation = {
  registerUser: (user) => {
    const validateObject = Joi.object({
      fullName: Joi.string().min(3).required(),
      emailId: Joi.string().email().lowercase().required(),
      password: Joi.string().min(6).required(),
    });
    return validateObject.validateAsync(user);
  },

  logInUser: (user) => {
    const validateObject = Joi.object({
      emailId: Joi.string().email().lowercase().required(),
      password: Joi.string().min(6).required(),
    });
    return validateObject.validateAsync(user);
  },
};

module.exports = Validation;
